(function ($) {
    "use strict";

    function listMsg(arr) {
        var dom = "",
            dom_alter = "",
            dom_comment = "",
            dom_like = "";
        arr.forEach(function (element, index) {
            var temp = "",
                style = "";
            if (element.hasComment && element.hasLike) {
                style = 'style="padding-top:0"';
            }
            if (element.hasAlter) {
                element.alter.forEach(function (ele, idx) {
                    if (ele.split(".")[1] === "mp4") {
                        temp += '<video src="video/' + ele + '" controls>';
                    } else {
                        temp += '<img src="image/' + ele + '" alt="">';
                    }
                });
                dom_alter =
                    '<div class="alter">' + temp + '</div>';
                temp = "";
            }
            if (element.hasComment) {
                element.comment.forEach(function (ele, idx) {
                    temp += '<p><span>' + ele.split("：")[0] + '：</span>' + ele.split("：")[1] + '</p>';
                });
                dom_comment =
                    '<div class="comm" ' + style + '>' + temp + '</div>';
                temp = "";
            }
            if (element.hasLike) {
                element.like.forEach(function (ele, idx) {
                    if (idx) {
                        temp += '，' + ele;
                    } else {
                        temp += ele;
                    }
                });
                dom_like =
                    '<div class="like"><p><span></span>' + temp + '</p></div>';
                temp = "";
            }
            dom +=
                '<div class="msg">' +
                '<img src="image/' + element.avatar + '" alt="" class="avatar">' +
                '<p class="nickname">' + element.nickname + '</p>' +
                '<p class="text">' + element.text + '</p>' + dom_alter +
                '<p class="date">' + element.date + '</p>' +
                dom_like +
                dom_comment +
                '</div>';
            dom_alter = "";
            dom_comment = "";
            dom_like = "";
        });
        $(".content").append(dom);
    }

    listMsg([
        {
            hasAlter: true,
            hasComment: true,
            hasLike: true,
            avatar: "avatar_qt.jpg",
            nickname: "千兔",
            text: "兔爷动漫三周年啦，we are family!",
            date: "2016年8月26日",
            alter: [
                "160826.jpg"
            ],
            comment: [
                "萝比：看起来好温馨哦(*≧▽≦*)",
                "千龙网：兔爷你全家福要不要这么fashion ！！"
            ],
            like: [
                "千兔", "小千", "小白人", "萝比", "佩哥", "兔爷", "等"
            ]
        },
        {
            hasAlter: true,
            hasComment: true,
            hasLike: false,
            avatar: "avatar_xq.jpg",
            nickname: "小千",
            text: "2016“情画北京”大学生手绘北京大赛正式启动，促进行业内的交流、更好的宣传北京。并收到来自各个学校同学的画作。",
            date: "2016年7月",
            alter: [
                "160701.jpg",
                "qhbj.mp4"
            ],
            comment: [
                "佩哥：原来咱们大北京也可以有这样的美！",
                "豆腐：看的我都想参加了，画个兔子怎么样？"
            ]
        },
        {
            hasAlter: true,
            hasComment: true,
            hasLike: false,
            avatar: "avatar_pg.jpg",
            nickname: "佩哥",
            text: "7月27日，千龙网与中国文化网络传播研究会共同发布网络文明系列动画《网语萌说》，兔爷动漫负责动画的全程创作。<br>7月22日，千龙网与中国出版集团签署合作协议，齐心发展动漫业态。",
            date: "2016年7月",
            alter: [
                "wyms.mp4"
            ],
            comment: [
                "小千：看完宝宝已不能好好说话了……",
                "爷爷：看来我是真的老了~规范一下还是很有必要啊！"
            ]
        },
        {
            hasAlter: true,
            hasComment: true,
            hasLike: false,
            avatar: "avatar_qt.jpg",
            nickname: "千兔",
            text: "制作七一特别栏目“与首都党员的聊天记录”。",
            date: "2016年6月",
            alter: [
                "160601.jpg"
            ],
            comment: [
                "千龙网：红星闪闪放光彩，红星灿灿暖胸怀……",
                "小白人：没有共产党就没有新中国，^O^ 吼吼吼……"
            ]
        },
        {
            hasAlter: true,
            hasComment: true,
            hasLike: false,
            avatar: "avatar_xq.jpg",
            nickname: "小千",
            text: "兔爷动漫漫画家与外国漫画家“1+1”结对画北京，促进北京的国际交流。",
            date: "2016年5月",
            alter: [
                "160501.jpg"
            ],
            comment: [
                "爷爷：漫画也可以促进中外交流，给你32个赞",
                "陈源：我画的大手有没有很搞笑"
            ]
        },
        {
            hasAlter: true,
            hasComment: true,
            hasLike: false,
            avatar: "avatar_xbr.jpg",
            nickname: "小白人",
            text: "《小白人》系列追忆80年代情怀~",
            date: "2016年4月6日",
            alter: [
                "160406.jpg"
            ],
            comment: [
                "佩哥：还蛮搞笑的哎，打算追着看",
                "蟾蜍：终于来了个比我还丑的！",
                "鸡爷：欢迎加入屌丝俱乐部~"
            ]
        },
        {
            hasAlter: true,
            hasComment: true,
            hasLike: false,
            avatar: "avatar_rb.jpg",
            nickname: "萝比",
            text: "萝比和佩哥出生啦！",
            date: "2016年3月24日",
            alter: [
                "160324.jpg"
            ],
            comment: [
                "小白人：原来你们就是萝比和佩哥啊",
                "兔爷：萝比小姐~请问你电话多少?~(¯﹃¯)~"
            ]
        },
        {
            hasAlter: true,
            hasComment: false,
            hasLike: false,
            avatar: "avatar_xq.jpg",
            nickname: "小千",
            text: "追踪时事热点的新闻动画《道听兔说》上线。",
            date: "2016年3月",
            alter: [
                "dtts.mp4"
            ]
        },
        {
            hasAlter: true,
            hasComment: true,
            hasLike: false,
            avatar: "avatar_qt.jpg",
            nickname: "千兔",
            text: "宣传片《兔爷你好》出炉。",
            date: "2015年12月",
            alter: [
                "tynh.mp4"
            ],
            comment: [
                "千兔：哈哈哈，bigbang的背景乐~"
            ]
        },
        {
            hasAlter: true,
            hasComment: true,
            hasLike: false,
            avatar: "avatar_pg.jpg",
            nickname: "佩哥",
            text: "新闻类漫画H5《兔版头条》上线，以创新的形式解读新闻。",
            date: "2015年12月",
            alter: [
                "151201.jpg"
            ],
            comment: [
                "鸡爷：一份会说话的报纸呐"
            ]
        },
        {
            hasAlter: true,
            hasComment: false,
            hasLike: true,
            avatar: "avatar_xbr.jpg",
            nickname: "小白人",
            text: "创作《温度》系列漫画，主在挖掘北京不一样的暖心瞬间，弘扬正能量。",
            date: "2015年12月",
            alter: [
                "151202.jpg"
            ],
            like: [
                "鸡爷", "小恐龙", "小白人"
            ]
        },
        {
            hasAlter: true,
            hasComment: false,
            hasLike: false,
            avatar: "avatar_rb.jpg",
            nickname: "萝比",
            text: "真人与3D创意动画《激情厨房》上线。",
            date: "2015年9月29日",
            alter: [
                "jqcf.mp4"
            ]
        },
        {
            hasAlter: true,
            hasComment: false,
            hasLike: false,
            avatar: "avatar_rb.jpg",
            nickname: "萝比",
            text: "原创动画《包你喜欢》推出。以一分钟的形式介绍主题，将真人与动画结合，以现代视角畅谈中国传统文化。",
            date: "2015年9月24日",
            alter: [
                "150924.jpg"
            ]
        },
        {
            hasAlter: true,
            hasComment: false,
            hasLike: true,
            avatar: "avatar_qt.jpg",
            nickname: "千兔",
            text: "兔爷动漫两周年啦！",
            date: "2015年8月26日",
            alter: [
                "150826.jpg"
            ],
            like: [
                "鸡爷", "小恐龙", "小白人", "千兔", "小千", "欢喜坨", "萝比", "佩哥", "兔爷", "等"
            ]
        },
        {
            hasAlter: true,
            hasComment: false,
            hasLike: false,
            avatar: "avatar_xq.jpg",
            nickname: "小千",
            text: "推出网络新闻动漫栏目《漫说新语》，涉及时事、文体、社会、生活、热点话题等，专注用动漫绘画语言表达诠释真善美、抨击假丑恶，传递正能量，致力于打造成为国内领先的网络新闻动漫原创高地和网络新闻动漫作品分享交流平台。",
            date: "2015年8月17日",
            alter: [
                "150817.jpg"
            ]
        },
        {
            hasAlter: true,
            hasComment: false,
            hasLike: false,
            avatar: "avatar_rb.jpg",
            nickname: "萝比",
            text: "互联网寓言动漫《千粟》正式上线。",
            date: "2015年7月",
            alter: [
                "qs.mp4"
            ]
        },
        {
            hasAlter: true,
            hasComment: true,
            hasLike: false,
            avatar: "avatar_pg.jpg",
            nickname: "佩哥",
            text: "原创公益动画《一头犀牛的自白》推出。",
            date: "2015年6月16日",
            alter: [
                "zb.mp4"
            ],
            comment: [
                "小白人：感动，泪目"
            ]
        },
        {
            hasAlter: true,
            hasComment: true,
            hasLike: false,
            avatar: "avatar_xbr.jpg",
            nickname: "小白人",
            text: "原创动画《早高峰之战》在各视频网站创百万点击。",
            date: "2015年4月22日",
            alter: [
                "zgfzz.mp4"
            ],
            comment: [
                "萝比：马里奥的上班之路真不易",
                "佩哥：里面好多经典动漫人物啊"
            ]
        },
        {
            hasAlter: true,
            hasComment: false,
            hasLike: false,
            avatar: "avatar_xbr.jpg",
            nickname: "小白人",
            text: "创作北京旅游类漫画《豆腐游京传》、历史名茶故事类漫画《兔爷茶馆》。",
            date: "2014年11月",
            alter: [
                "141101.jpg"
            ]
        },
        {
            hasAlter: false,
            hasComment: false,
            hasLike: false,
            avatar: "avatar_xq.jpg",
            nickname: "小千",
            text: "创作漫画版图表新闻《香港“占中”十问》，并由中央电视台《新闻联播》播发新闻。",
            date: "2014年10月"
        },
        {
            hasAlter: true,
            hasComment: true,
            hasLike: false,
            avatar: "avatar_qt.jpg",
            nickname: "千兔",
            text: "原创中国首部3D折纸微动画《智慧北京》，用创新的方式突出北京城市建筑、人文情怀。",
            date: "2014年9月",
            alter: [
                "zhbj.mp4"
            ],
            comment: [
                "豆腐：好看好玩好新奇！"
            ]
        },
        {
            hasAlter: true,
            hasComment: false,
            hasLike: false,
            avatar: "avatar_xq.jpg",
            nickname: "小千",
            text: "根据首都精神文明办启动的“迎接APEC，精彩北京人”市民群众文明实践活动精神，推出《行行讲文明 迎接APEC》文明礼仪主题漫画。",
            date: "2014年6月",
            alter: [
                "140601.jpg"
            ]
        },
        {
            hasAlter: true,
            hasComment: false,
            hasLike: false,
            avatar: "avatar_pg.jpg",
            nickname: "佩哥",
            text: "创作《党政领导干部廉政新规图解》，并印刷成册发至北京市全市处级以上领导干部。",
            date: "2014年6月",
            alter: [
                "140602.jpg"
            ]
        },
        {
            hasAlter: true,
            hasComment: false,
            hasLike: false,
            avatar: "avatar_xbr.jpg",
            nickname: "小白人",
            text: "漫画版图表新闻《美国全球监听行动纪录》，入编人民出版社出版的同名书籍。",
            date: "2014年5月",
            alter: [
                "140501.jpg"
            ]
        },
        {
            hasAlter: true,
            hasComment: false,
            hasLike: true,
            avatar: "avatar_rb.jpg",
            nickname: "萝比",
            text: "创作漫画版图表新闻《习主席的时间都去哪儿了？》，首创发布习近平主席漫画形象，成为中国新闻媒体报道领导人形式的一个“开创性”案例。并持续跟进制作“习主席的时间”系列。",
            date: "2014年2月7日",
            alter: [
                "140207.jpg"
            ],
            like: [
                "千兔", "小千", "欢喜坨", "鸡爷", "小恐龙", "小白人", "萝比", "佩哥", "兔爷", "等"
            ]
        },
        {
            hasAlter: true,
            hasComment: true,
            hasLike: false,
            avatar: "avatar_qt.jpg",
            nickname: "千兔",
            text: "制作完成嫦娥三号动态专题。",
            date: "2014年2月3日",
            alter: [
                "140203.jpg"
            ],
            comment: [
                "欢喜坨：哇，好高端~！"
            ]
        },
        {
            hasAlter: true,
            hasComment: false,
            hasLike: true,
            avatar: "avatar_xbr.jpg",
            nickname: "小白人",
            text: "创建千龙网图表新闻频道，并结合漫画，着力打造特色数据类新闻。",
            date: "2014年1月",
            alter: [
                "140101.jpg"
            ],
            like: [
                "千兔", "小千", "豆腐"
            ]
        },
        {
            hasAlter: true,
            hasComment: true,
            hasLike: false,
            avatar: "avatar_qt.jpg",
            nickname: "千兔",
            text: "原创策划、编导、制作长篇励志漫画剧《京城千兔》上线",
            date: "2013年11月8日",
            alter: [
                "131108_1.jpg",
                "131108_2.jpg"
            ],
            comment: [
                "小千：可以看连载漫画啦！"
            ]
        },
        {
            hasAlter: true,
            hasComment: true,
            hasLike: false,
            avatar: "avatar_xq.jpg",
            nickname: "小千",
            text: "“兔爷动漫”频道正式上线，是国内新闻网站首个以民间吉祥物命名和主持的原创动漫传播平台。",
            date: "2013年8月26日",
            alter: [
                "130826.jpg"
            ],
            comment: [
                "千兔：哇，兔爷动漫上线了，棒棒棒！"
            ]
        }
    ]);

    function ending(v) {
        var f = $(".footer"),
            c = $(".content"),
            a = $("audio");
        c.animate({
            "opacity": "0"
        }, 2000 * v);
        f.animate({
            "opacity": "1"
        }, 2000 * v);
        a.prop("loop", false);
    }

    function jump(v) {
        var x = 0,
            t = setInterval(function () {
                $("body").animate({
                    scrollTop: $(".msg").eq(x).offset().top - 120
                }, 1500 * v);
                [1, 2, 7, 8, 11, 15, 16, 17, 20].forEach(function (element, index) {
                    if (x === element) {
                        $("video").eq(index)[0].play();
                    }
                    if (x === element + 1) {
                        $("video").eq(index)[0].pause();
                    }
                });
                if (x === $(".msg").length - 1) {
                    clearInterval(t);
                    setTimeout(function () {
                        ending(1);
                    }, 4000 * v);
                }
                x += 1;
            }, 4000 * v);
    }

    function opening(v) {
        var f = $(".foreword"),
            a = $("audio"),
            b = $("body");
        f.find("img").each(function (index, element) {
            $(element).delay(index * 2500 * v).animate({
                "opacity": "1"
            }, 500 * v);
        });
        f.delay(10000).animate({
            opacity: "0"
        }, 1000 * v, function () {
            jump(1);
            a.prop("loop", true);
            a[0].src = "music/bgm_main.mp3";
            b.css({
                "overflow": "auto"
            });
        });
    }

    opening(1);

}(window.jQuery));